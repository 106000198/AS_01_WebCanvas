# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
1、點擊顏色板，改變包括畫筆、文字和圖形工具的顏色。
2、拖動滑動條，改變畫筆大小或字體大小。
3、點擊畫筆圖標，進入普通畫筆模式；鼠標變成自定義畫筆。
4、點擊橡皮圖標，進入橡皮模式；鼠標變成自定義橡皮。
5、點擊文字圖標，先在畫板上點擊一次選擇位置，然後在彈出窗口輸入文字，文字顯示在選擇的位置上；鼠標變成文字輸入模式。
6、點擊文字菜單圖標，在彈出菜單裡選擇字體，可以點擊右上角叉號關閉菜單。
7、點擊圓形圖標，畫圓形；長方形圖標，畫長方形；三角形圖標，畫三角形。鼠標變成十字。
8、點擊'choose file'，在彈出窗口選擇本地文件，然後在畫板上點擊一次，選擇圖片顯示位置。
9、點擊箭頭圖標，撤銷一次操作；點擊箭頭圖標，反撤銷一次操作。只要在畫版上新添內容，就無法做反撤銷操作。
10、點擊下載圖標，下載圖片，圖片名稱為“download+編碼”，編碼從1開始。
11、點擊雙箭頭圖標，清空畫板。清空畫板後歷史紀錄也清空，無法做撤銷反撤銷操作。
12、額外功能
        點擊平行四邊形圖標，畫平行四邊形。
        點擊彩虹畫筆圖標，畫彩虹。


