//工具
var BRUSH = document.getElementById('brush');
var COLOR = document.getElementById('color');
var TEXT = document.getElementById('text');
var CIRCLE = document.getElementById('circle');
var TRIANGLE = document.getElementById('triangle');
var UNDO = document.getElementById('undo');
var DOWNLOAD = document.getElementById('download');
var ERASER = document.getElementById('eraser');
var BRUSH_SIZE = document.getElementById('brush_size');
var FONT = document.getElementById('font');
var RECTANGLE = document.getElementById('rectangle');
// var UPLOAD = document.getElementById('upload');
var REDO = document.getElementById('redo');
var RESET = document.getElementById('reset');

var PARALLEL = document.getElementById("parallel");
var NEW_BRUSH= document.getElementById("newbrush");
var operations = [BRUSH,TEXT,CIRCLE,TRIANGLE,ERASER,RECTANGLE,PARALLEL,NEW_BRUSH];

var CANVAS = document.getElementById('canvas');
var ctx = CANVAS.getContext("2d");

//調色盤
var COLOR_palette1 = document.getElementById("color_palette1");
var color_ctx1 = COLOR_palette1.getContext("2d");
var COLOR_palette2 = document.getElementById("color_palette2");
var color_ctx2 = COLOR_palette2.getContext("2d");
var color_select = 'black';

//拖動條
var BRUSH_size = document.getElementById("brush_size");
var BRUSH_show = document.getElementById("brushsize_show");
BRUSH_show.innerHTML = BRUSH_size.value;

var TEXT_size = document.getElementById("text_size");
var TEXT_show = document.getElementById("textsize_show");
TEXT_show.innerHTML = TEXT_size.value;

//歷史紀錄
var canvasHistory = [];
var step = 0;
canvasHistory.push(ctx.getImageData(0, 0, CANVAS.width, CANVAS.height));

//上傳圖片
var Imageload = document.getElementById("imageload");
//下載圖片
var countnum = 1;


//font menu
var FONT_sidebar = document.getElementById("font_sidebar");
var FONT_close = document.getElementById("font_close");
var Serif = document.getElementById("serif");
var Sansserif = document.getElementById("sans-serif");
var Cursive = document.getElementById("cursive");
var Fantasy = document.getElementById("fantasy");
var Monospace = document.getElementById("monospace");
var fonts = [Serif,Sansserif,Cursive,Fantasy,Monospace];




window.onload = function(){
    build_pallete1();
    build_pallete2();
    setfont('sans-serif',1);
}

//選中某一工具時改變網頁顯示
function display_board(num){
    for(var i=0; i<operations.length; i++){
        if(i!=num){
            operations[i].style.background = "#ffffff00";
        }
        else{
            operations[i].style.background = "#FAFAD2";
        }
        }
    }
    

function setbrush(){
    display_board(0);
    CANVAS.style.cursor = 'url("https://img.icons8.com/ios-glyphs/2x/paint.png"), auto';
    var flag = 0;//mouse flag
    CANVAS.onmousedown = function(e){
        flag = 1;
        var start_x = e.pageX - this.offsetLeft;
        var start_y = e.pageY - this.offsetTop;
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
    }
    CANVAS.onmousemove = function(e){
         if(flag){
        var next_x = e.pageX - this.offsetLeft;
        var next_y = e.pageY - this.offsetTop;
        ctx.lineTo(next_x,next_y);
        ctx.stroke();
         }
    }
    CANVAS.onmouseup = function(e){
        flag = 0;
        var imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
        if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
        canvasHistory.push(imgData);
        step++;
   }
   CANVAS.onmouseout = function(e){
        flag = 0;
    }

}

function seteraser(){
    display_board(4);
    CANVAS.style.cursor = 'url("https://img.icons8.com/metro/2x/erase.png"), auto';
    var flag = 0;//mouse flag
    CANVAS.onmousedown = function(e){
        flag = 1;
        var start_x = e.pageX - this.offsetLeft;
        var start_y = e.pageY - this.offsetTop;
        ctx.clearRect(start_x-4*ctx.lineWidth,start_y-4*ctx.lineWidth,8*ctx.lineWidth,8*ctx.lineWidth);
    }
    CANVAS.onmousemove = function(e){
         if(flag){
        var next_x = e.pageX - this.offsetLeft;
        var next_y = e.pageY - this.offsetTop;
        ctx.clearRect(next_x-4*ctx.lineWidth,next_y-4*ctx.lineWidth,8*ctx.lineWidth,8*ctx.lineWidth);
         }
    }
    CANVAS.onmouseup = function(e){
        flag = 0;
        var imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
        if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
        canvasHistory.push(imgData);
        step++;
   }
   CANVAS.onmouseout = function(e){
        flag = 0;
    }

}

function settext(){
    display_board(1);
    CANVAS.style.cursor = 'text';
    CANVAS.onmouseup = function(e){
    var text_x = e.pageX - this.offsetLeft;
    var text_y = e.pageY - this.offsetTop;
    var result = window.prompt('請在這裡輸入...', '');
    if(result!=null) ctx.fillText(result, text_x, text_y);

    var imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
    if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
    canvasHistory.push(imgData);
    step++;
    }

}

//font menu
function openmenu_font(){
    FONT_sidebar.style.width= "90px";
}
function closemenu_font(){
    FONT_sidebar.style.width = "0";   
}
function setfont(description,num){
    ctx.font = TEXT_size.value + 'px ' + description;
    if(description!=''){
    for(var i=0; i<5; i++){
        if(i==num) {
            fonts[i].style.border = "1px solid darkred";
        }
        else fonts[i].style.border = "0";
    }
  }
}//font menu

function build_pallete1(){
    var gradient = color_ctx1.createLinearGradient(0, 0, COLOR_palette1.width, 0);
    gradient.addColorStop(0, "rgb(255, 0, 0)");
    gradient.addColorStop(0.16, "rgb(255, 255, 0)");
    gradient.addColorStop(0.33, "rgb(0, 255, 0)");
    gradient.addColorStop(0.49, "rgb(0, 255, 255)");
    gradient.addColorStop(0.66, "rgb(0, 0, 255)");
    gradient.addColorStop(0.83, "rgb(255, 0, 255)");
    gradient.addColorStop(1, "rgb(255, 0, 0)");

    color_ctx1.fillStyle = gradient;
    color_ctx1.fillRect(0, 0, color_ctx1.canvas.width, color_ctx1.canvas.height);
}
function build_pallete2(){
    var gradient = color_ctx2.createLinearGradient(0, 0, 0, COLOR_palette2.height);
    gradient.addColorStop(0, "rgb(0, 0, 0)");
    gradient.addColorStop(1, "rgb(255, 255, 255)");

    color_ctx2.fillStyle = gradient;
    color_ctx2.fillRect(0, 0, color_ctx2.canvas.width, color_ctx2.canvas.height);
}
function getcolor1(event){
    var color_x = event.clientX - COLOR_palette1.offsetLeft;
    var color_y = event.clientY - COLOR_palette1.offsetTop;

    imagedata = color_ctx1.getImageData(color_x, color_y, 1, 1);
    color_select = 'rgb(' + imagedata.data[0] + ', ' + imagedata.data[1] + ', ' + imagedata.data[2] + ')'; 
    ctx.strokeStyle = color_select;
    ctx.fillStyle = color_select;
}
function getcolor2(event){
    var color_x = event.clientX - COLOR_palette2.offsetLeft;
    var color_y = event.clientY - COLOR_palette2.offsetTop;

    imagedata = color_ctx2.getImageData(color_x, color_y, 1, 1);
    color_select = 'rgb(' + imagedata.data[0] + ', ' + imagedata.data[1] + ', ' + imagedata.data[2] + ')'; 
    ctx.strokeStyle = color_select;
    ctx.fillStyle = color_select;
}

BRUSH_size.oninput = function() {
    ctx.lineWidth = BRUSH_size.value;
    BRUSH_show.innerHTML = BRUSH_size.value;
}
TEXT_size.oninput = function() {
    var fontlast = ctx.font.split(' ');
    var newSize = TEXT_size.value + 'px';
    ctx.font = newSize + ' ' + fontlast[1];
    TEXT_show.innerHTML = TEXT_size.value;
}



function drawcir(){
    var cstart_x,cstart_y;
    var imgData;

    display_board(2);
    CANVAS.style.cursor = 'crosshair';
    var flag = 0;//mouse flag
    CANVAS.onmousedown = function(e){
        flag = 1;
        cstart_x = e.pageX - this.offsetLeft;
        cstart_y = e.pageY - this.offsetTop;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
    }
    CANVAS.onmousemove = function(e){
         if(flag){
        var next_x = e.pageX - this.offsetLeft;
        var next_y = e.pageY - this.offsetTop;
        var dx = next_x - cstart_x;
        var dy = next_y - cstart_y;
        var r = (Math.sqrt(dx*dx+dy*dy))/2;
        var arcx = (cstart_x + next_x)/2;
        var arcy = (cstart_y + next_y)/2;

        ctx.clearRect(0, 0, CANVAS.width, CANVAS.height);
        ctx.putImageData(imgData, 0, 0);
        
        ctx.beginPath();
        ctx.arc(arcx, arcy, r, 0, 360, false);
		ctx.closePath();
		ctx.stroke();
         }
    }
    CANVAS.onmouseup = function(e){
        flag = 0;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
        if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
        canvasHistory.push(imgData);
        step++;
   }
   CANVAS.onmouseout = function(e){
        flag = 0;
    }

}

function drawtri(){
    var start_x,start_y;
    var point_x, point_y;
    var tem_x, tem_y;
    var imgData;

    display_board(3);
    CANVAS.style.cursor = 'crosshair';
    var flag = 0;//mouse flag
    CANVAS.onmousedown = function(e){
        flag = 1;
        start_x = e.pageX - this.offsetLeft;
        start_y = e.pageY - this.offsetTop;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
    }
    CANVAS.onmousemove = function(e){
         if(flag){
        var next_x = e.pageX - this.offsetLeft;
        var next_y = e.pageY - this.offsetTop;
        point_x = start_x;
        point_y = next_y;//左下角頂點
        tem_x = (start_x+next_x)/2;
        tem_y = start_y;
        

        ctx.clearRect(0, 0, CANVAS.width, CANVAS.height);
        ctx.putImageData(imgData, 0, 0);
        
        ctx.beginPath();
        ctx.moveTo(point_x,point_y);
        ctx.lineTo(tem_x,tem_y);
        ctx.lineTo(next_x,next_y);
        ctx.closePath();
        ctx.stroke();
         }
    }
    CANVAS.onmouseup = function(e){
        flag = 0;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
        if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
        canvasHistory.push(imgData);
        step++;
   }
   CANVAS.onmouseout = function(e){
        flag = 0;
    }

}

function drawrec(){
    var start_x,start_y;
    var imgData;

    display_board(5);
    CANVAS.style.cursor = 'crosshair';
    var flag = 0;//mouse flag
    CANVAS.onmousedown = function(e){
        flag = 1;
        start_x = e.pageX - this.offsetLeft;
        start_y = e.pageY - this.offsetTop;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
    }
    CANVAS.onmousemove = function(e){
         if(flag){
        var next_x = e.pageX - this.offsetLeft;
        var next_y = e.pageY - this.offsetTop;
        var dx = next_x - start_x;
        var dy = next_y - start_y;

        ctx.clearRect(0, 0, CANVAS.width, CANVAS.height);
        ctx.putImageData(imgData, 0, 0);
        
        ctx.strokeRect(start_x, start_y, dx, dy);
         }
    }
    CANVAS.onmouseup = function(e){
        flag = 0;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
        if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
        canvasHistory.push(imgData);
        step++;
   }
   CANVAS.onmouseout = function(e){
        flag = 0;
    }

}

UNDO.addEventListener('click', function(){
    display_board(-1);
    CANVAS.style.cursor = 'default';
    CANVAS.onmousedown = function(e){}

    if(step>0){
        ctx.putImageData(canvasHistory[step-1], 0, 0);
        step--;
        }   
})

REDO.addEventListener('click', function(){
    display_board(-1);
    CANVAS.style.cursor = 'default';
    CANVAS.onmousedown = function(e){}

    if(step<canvasHistory.length-1){
    ctx.putImageData(canvasHistory[step+1], 0, 0);
    step++;
    }
})

Imageload.addEventListener('change', function(){
    display_board(-1);
    CANVAS.style.cursor = 'default';
    CANVAS.onmousedown = function(e){}
    


    if (this.files && this.files[0] ) {
        var reader= new FileReader();
        reader.onload = function(E) {
           var Img = new Image();
           Img.onload = function() {
            CANVAS.onmouseup = function(e){
            ctx.drawImage(Img, 0, 0, Img.width, Img.height, e.pageX-CANVAS.offsetLeft, e.pageY-CANVAS.offsetTop, Img.width, Img.height);
            }
           };
           Img.src = E.target.result;
        };       
        reader.readAsDataURL(this.files[0]);
    }
})

DOWNLOAD.addEventListener('click', function(){
    display_board(-1);
    CANVAS.style.cursor = 'default';
    CANVAS.onmousedown = function(e){}

    var Img = CANVAS.toDataURL("image/png");

 var save_link = document.createElement('a');
     save_link.href = Img;
     save_link.download ='download' + countnum + '.png';
                           
 var clickevent = document.createEvent('MouseEvents');
     clickevent.initEvent('click', true, false);
     save_link.dispatchEvent(clickevent);

     countnum++;
})

function clearpic(){
    display_board(-1);
    CANVAS.style.cursor = 'default';
    CANVAS.onmousedown = function(e){}

    ctx.clearRect(0,0,CANVAS.width,CANVAS.height);
    canvasHistory.splice(1,canvasHistory.length-1);
    step = 0;
}

//add
function drawparallel(){
    var start_x,start_y;
    var up_x, up_y, down_x, down_y;
    var imgData;

    display_board(6);
    CANVAS.style.cursor = 'crosshair';
    var flag = 0;//mouse flag
    CANVAS.onmousedown = function(e){
        flag = 1;
        start_x = e.pageX - this.offsetLeft;
        start_y = e.pageY - this.offsetTop;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
    }
    CANVAS.onmousemove = function(e){
         if(flag){
        var next_x = e.pageX - this.offsetLeft;
        var next_y = e.pageY - this.offsetTop;
        up_x = (start_x+next_x)/2;
        up_y = start_y;
        down_x = (start_x+next_x)/2;
        down_y = next_y;

        ctx.clearRect(0, 0, CANVAS.width, CANVAS.height);
        ctx.putImageData(imgData, 0, 0);
        
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
        ctx.lineTo(up_x,up_y);
        ctx.lineTo(next_x,next_y);
        ctx.lineTo(down_x,down_y);
        ctx.closePath();
        ctx.stroke();
        
        }
    }
    CANVAS.onmouseup = function(e){
        flag = 0;
        imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
        if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
        canvasHistory.push(imgData);
        step++;
   }
   CANVAS.onmouseout = function(e){
        flag = 0;
    }
}

function newbrush(){
    display_board(7);
    CANVAS.style.cursor = 'url("https://img.icons8.com/ios-glyphs/2x/paint.png"), auto';

    var start_x, start_y;
    var flag = 0;//mouse flag
    CANVAS.onmousedown = function(e){
        flag = 1;
        start_x = e.pageX - this.offsetLeft;
        start_y = e.pageY - this.offsetTop;
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
    }
    CANVAS.onmousemove = function(e){
         if(flag){
        var next_x = e.pageX - this.offsetLeft;
        var next_y = e.pageY - this.offsetTop;

        var gradient = ctx.createLinearGradient(start_x, start_y, next_x, next_y);
        gradient.addColorStop(0, "rgb(255, 0, 0)");
        gradient.addColorStop(0.2, "rgb(255, 255, 0)");
        gradient.addColorStop(0.4, "rgb(0, 255, 0)");
        gradient.addColorStop(0.6, "rgb(0, 255, 255)");
        gradient.addColorStop(0.8, "rgb(0, 0, 255)");
        gradient.addColorStop(1, "rgb(255, 0, 255)");
        ctx.strokeStyle = gradient;
        ctx.lineTo(next_x,next_y);
        ctx.stroke();
         }
    }
    CANVAS.onmouseup = function(e){
        flag = 0;
        var imgData = ctx.getImageData(0, 0, CANVAS.width, CANVAS.height);
        if(step<canvasHistory.length-1) canvasHistory.splice(step+1, canvasHistory.length-1-step);
        canvasHistory.push(imgData);
        step++;
        ctx.strokeStyle = color_select;
   }
   CANVAS.onmouseout = function(e){
        flag = 0;
    }
}
